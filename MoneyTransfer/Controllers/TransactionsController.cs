﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransactionsController : Controller
    {
        private readonly UserManager<User> _userManager;
        public ApplicationContext context;

        public TransactionsController(UserManager<User> userManager, ApplicationContext context)
        {
            _userManager = userManager;
            this.context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult CreateTransaction()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateTransaction(TransactionsViewModel model)
        {
            if (ModelState.IsValid)
            {
                User userOut = await _userManager.FindByNameAsync(model.PersonalAccountOut.ToString());
                User userIn = await _userManager.FindByNameAsync(model.PersonalAccountIn.ToString());

                if (userOut.Balance >= model.Summa)
                {
                    userOut.Balance = userOut.Balance - model.Summa;
                    userIn.Balance = userIn.Balance + model.Summa;
                    Transactions transactions = new Transactions()
                    {
                        PersonalAccountOut = userOut.Id,
                        PersonalAccountIn = userIn.Id,
                        Summa = model.Summa,
                        DateTime = DateTime.Now.ToString("MM/dd/yyyy")
                    };

                    _userManager.UpdateAsync(userIn);
                    _userManager.UpdateAsync(userOut);
                    context.Transactionses.Update(transactions);
                    await context.SaveChangesAsync();
                }
            }

            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult ReplenishmentOfFunds()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ReplenishmentOfFunds(TransactionsViewModel model)
        {
            User userIn = await _userManager.FindByNameAsync(model.PersonalAccountIn.ToString());
            userIn.Balance = userIn.Balance + model.Summa;
            await _userManager.UpdateAsync(userIn);
            Transactions transactions = new Transactions()
            {
                PersonalAccountIn = userIn.UserName,
                Summa = model.Summa,
                DateTime = DateTime.Now.ToString("MM/dd/yyyy")
            };
            context.Update(transactions);
            await context.SaveChangesAsync();
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> MyTranzaction()
        {
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            IQueryable<Transactions> transactions = context.Transactionses.Where(t => t.PersonalAccountIn == user.UserName.ToString() || t.PersonalAccountOut == user.UserName.ToString());
            return View(transactions);
        }
    }
}