﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transactions
    {
        [Key]
        public int Id { get; set; }
        public string PersonalAccountOut { get; set; }
        public int Summa { get; set; }
        public string PersonalAccountIn { get; set; }
        public string DateTime { get; set; }
    }
}
