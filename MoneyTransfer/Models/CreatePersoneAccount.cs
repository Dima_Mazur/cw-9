﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class CreatePersoneAccount
    {
        public ApplicationContext context;

        private Random rnd = new Random();

        public int Create()
        {
            int[] PersonalAccount = new int[6];
            for (int i = 0; i < PersonalAccount.Length; i++)
            {
                int number = rnd.Next(0, 10);
                PersonalAccount[i] = number;
            }
            int PersonAC = Convert.ToInt32(String.Join("", PersonalAccount));

            return PersonAC;
        }

        public int CheckPersonalAccount()
        {
            int PersonAC = Create();

            try
            {
                User user = context.Users.FirstOrDefault(u => u.PersonalAccount == PersonAC);
                if (user == null)
                {
                    PersonAC = Create();
                }
            }
            catch (Exception e)
            {
                
            }
            
            return PersonAC;
        }
    }
}
