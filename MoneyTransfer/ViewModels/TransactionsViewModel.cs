﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class TransactionsViewModel
    {
        [Required]
        [Display(Name = "Лицевой счет отправителя")]
        public int PersonalAccountOut { get; set; }

        [Required]
        [Display(Name = "Сумма для отправки")]
        public int Summa { get; set; }

        [Required]
        [Display(Name = "Лицевой счет Получателя")]
        public int PersonalAccountIn { get; set; }

        public string ReturnUrl { get; set; }
    }
}
