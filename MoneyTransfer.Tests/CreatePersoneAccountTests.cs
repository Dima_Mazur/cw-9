﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using MoneyTransfer.Models;
using Xunit;

namespace MoneyTransfer.Tests
{
    public class CreatePersoneAccountTests
    {
        [Fact]
        public void CreatePersonalAccount()
        {
            CreatePersoneAccount createPersoneAccount = new CreatePersoneAccount();
            int? result = createPersoneAccount.Create() as int?;
            Assert.Equal(6, result?.ToString().Length);
        }

        [Fact]
        public void CreatePersonalAccountIsNotNull()
        {
            CreatePersoneAccount createPersoneAccount = new CreatePersoneAccount();
            int? result = createPersoneAccount.Create() as int?;
            Assert.NotNull(result);
        }

        [Fact]
        public void CheckPersonalAccountInDataBase()
        {

            CreatePersoneAccount createPersoneAccount = new CreatePersoneAccount();
            int? result = createPersoneAccount.CheckPersonalAccount() as int?;
            Assert.NotNull(result);
        }
    }
}
 